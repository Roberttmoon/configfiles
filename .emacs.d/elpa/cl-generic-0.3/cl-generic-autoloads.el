;;; cl-generic-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "cl-generic" "cl-generic.el" (22615 5654 78579
;;;;;;  298000))
;;; Generated autoloads from cl-generic.el
 (let ((d (file-name-directory #$)))
  (when (member d load-path)
    (setq load-path (append (remove d load-path) (list d)))))

;;;***

;;;### (autoloads nil nil ("cl-generic-pkg.el") (22615 5654 87479
;;;;;;  5000))

;;;***

(provide 'cl-generic-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; cl-generic-autoloads.el ends here
